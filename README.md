# A project to test Gitlab CI
 
[![build status](https://gitlab.cecs.anu.edu.au/u6736680/gitlab_ci_test/badges/master/build.svg)](https://gitlab.cecs.anu.edu.au/u6736680/gitlab_ci_test/commits/master)
***
Official website: https://about.gitlab.com/features/gitlab-ci-cd/  
Quick start: https://docs.gitlab.com/ee/ci/quick_start/  
Runner installation guide: https://docs.gitlab.com/runner/  
GitLab CI/CD Examples: https://docs.gitlab.com/ee/ci/examples/
